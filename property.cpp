// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#include "property.h"
#include <cassert>

namespace media_graph {

NamedProperty::NamedProperty(PropertyList* list, std::string name) : name_(std::move(name)) {
    list->addExternalProperty(this);
}

PropertyList::~PropertyList() = default;

int PropertyList::numProperty() const {
    return properties_.size();
}

NamedProperty* PropertyList::property(int id) {
    size_t id_unsigned = id;
    if (id_unsigned < properties_.size()) { return properties_.at(id_unsigned); }
    return nullptr;
}

NamedProperty* PropertyList::getPropertyByName(const std::string& name) {
    int num_props = numProperty();
    for (int i = 0; i < num_props; ++i) {
        NamedProperty* prop = property(i);
        assert(prop != 0);
        if (prop && (name == prop->name())) { return prop; }
    }
    return nullptr;
}

}  // namespace media_graph
