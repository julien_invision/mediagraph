#include "timestamp.h"

Timestamp Timestamp::now() {
    return Timestamp{
        std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::system_clock::now())
    };
}

std::ostream& operator<<(std::ostream& o, const Timestamp& t) { return o << t.asString() + " UTC"; }
