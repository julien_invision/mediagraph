// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#include "duration.h"

#include <sstream>
#include <thread>

void Duration::sleep() const {
    if (duration_ > 0) {
        std::this_thread::sleep_for(static_cast<std::chrono::microseconds>(duration_));
    }
}

namespace {
template <typename Ratio>
Duration printDurationAndGetRemainder(std::ostream& out, std::chrono::microseconds d,
                                     const char* unitName, const char* plural, bool& first) {
    using Unit = std::chrono::duration<int64_t, Ratio>;

    auto rounded = std::chrono::duration_cast<Unit>(d);
    if (rounded.count() == 0) {
        return d;
    } else {
        if (first) {
            first = false;
        } else {
            out << " ";
        }
        out << rounded.count() << unitName;
        if (rounded.count() > 1) { out << plural; }

        return d - rounded;
    }
}

}  // namespace

std::string Duration::format() const {
    if (duration_ == 0) { return "(0s)"; }

    std::stringstream ss;

    std::chrono::microseconds d = *this;
    if (duration_ < 0) {
        ss << "- ";
        d = -d;
    }
    ss << "(";
    bool first = true;
    // Those values are defined as constants in c++20.
    // https://en.cppreference.com/w/cpp/header/chrono#Convenience_Typedefs
    // But here, we want to support c++17 and we just need the ratio, not the duration,
    // so we copy the values.
    d = printDurationAndGetRemainder<std::ratio<31556952>>(ss, d, " year", "s", first);
    d = printDurationAndGetRemainder<std::ratio<2629746>>(ss, d, " month", "s", first);
    d = printDurationAndGetRemainder<std::ratio<604800>>(ss, d, " week", "s", first);
    d = printDurationAndGetRemainder<std::ratio<86400>>(ss, d, " day", "s", first);
    d = printDurationAndGetRemainder<std::ratio<3600>>(ss, d, "h", "", first);
    d = printDurationAndGetRemainder<std::ratio<60>>(ss, d, "m", "", first);
    d = printDurationAndGetRemainder<std::ratio<1>>(ss, d, "s", "", first);
    d = printDurationAndGetRemainder<std::milli>(ss, d, "ms", "", first);
    d = printDurationAndGetRemainder<std::micro>(ss, d, "us", "", first);
    ss << ")";

    return ss.str();
}
