// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#ifndef MEDIAGRAPH_NODE_H
#define MEDIAGRAPH_NODE_H

#include <set>
#include <string>

#include "property.h"
#include "thread_primitives.h"
#include "timestamp.h"

namespace media_graph {
class Graph;
class NamedStream;
class NamedPin;

/*! All nodes in the media graph derive from this class. Basically, it
 * provides reflection on pins.
 */
class NodeBase : public PropertyList {
public:
    NodeBase();

    virtual ~NodeBase();

    /// Tries to start the node.
    /// The default implementation verifies that all input pins are connected.
    /// It calls 'open()' on all output streams and all pins.
    /// Inheriting nodes can add more verification & initialization code by
    /// overloading this method.
    virtual bool start();

    /// Stops the node and disconnects all connected pins.
    /// Disconnecting all pins is necessary to make sure the graph can continue
    /// to run even if the node is stopped.
    virtual void stop();

    /// Returns true if start() has been called successfully.
    virtual bool isRunning() const;

    /// If started, blocks the calling thread until stop() is called.
    /// Otherwise: do nothing.
    virtual void waitUntilStopped();

    /// Returns the number of output streams that your node exposes.
    /// Any node exposing at least one output stream must overload this method.
    virtual int numOutputStream() const { return 0; }

    /// Returns a pointer to outputStream number <index>, or 0 if index is out
    /// of range.
    virtual const NamedStream* constOutputStream(int) const { return 0; }

    NamedStream* outputStream(int index) {
        return const_cast<NamedStream*>(this->constOutputStream(index));
    }

    /// Gets a stream by its name. Returns null if no stream has this name.
    virtual NamedStream* getOutputStreamByName(const std::string& name);

    /// Returns the number of input pins. Any node with at least one input pins
    /// must overload this method.
    virtual int numInputPin() const { return 0; }

    /// Returns a pointer to input pin number <index>, or null if index is out
    /// of range. Nodes with input pins must overload this method.
    virtual const NamedPin* constInputPin(int) const { return 0; };
    NamedPin* inputPin(int index) const {
        return const_cast<NamedPin*>(this->constInputPin(index));
    }

    /// Returns the pin named <name>, or null if there is no such pin.
    virtual NamedPin* getInputPinByName(const std::string& name);

    /// Wait for any input pin to receive new data. To know which one, iterate
    /// call tryRead on all input pins.
    /// Timeout of zero (default) means wait forever
    void waitForPinActivity(Duration timeout = {}) const;

    bool allPinsConnected() const;
    bool allPinsConnectedAndOpen() const;
    void openConnectedPins() const;
    void closeConnectedPins() const;
    void disconnectAllPins() const;
    void disconnectAllStreams();
    void openAllStreams();
    void closeAllStreams();

    void signalActivity() const;

    const std::string& name() const { return name_; }
    const std::set<Graph*>& graphs() const { return graphs_; }

    /// Returns false if the node has already been named and the input <name>
    /// is different or if trying to set the name to an empty string.
    bool setName(const std::string& name);

    /// Adds graph to list of graphs the node belongs to. Fails if if the node
    /// is already part of the given graph. Called by Graph::addNode() only.
    bool attach(Graph* new_graph);

    /// Only adds a node if its name can be set and it can be attached to the
    /// graph. Called by Graph::addNode() only.
    bool setNameAndAttach(const std::string& new_name, Graph* new_graph);

    bool isNamed();

    // unplug the node from all graphs.
    void detach();

    // unplug the node from one graphs.
    void detach(Graph* detach_graph);

private:
    bool canSetName(const std::string& new_name);

    mutable std::condition_variable pin_activity_;
    mutable std::mutex pin_activity_mutex_;

    mutable std::condition_variable stop_event_;
    mutable std::mutex stop_event_mutex_;

    std::set<Graph*> graphs_;
    std::string name_;
    bool running_;
    bool stopping_;
};

/*! Convenience class for nodes that need their own thread.
 *  To use, derive from ThreadedNodeBase and implement threadMain().
 */
class ThreadedNodeBase : public NodeBase {
public:
    enum class BehaviorOnThreadException { NORMAL_NODE_STOP, STOP_GRAPH, STOP_GRAPH_PROPAGATE_EXCEPTION };

    ThreadedNodeBase() = default;
    virtual ~ThreadedNodeBase();

    // Starts all output streams + the thread.
    [[nodiscard]] virtual bool start() override;

    // Stops the thread and all output streams.
    virtual void stop() override;

    virtual bool isRunning() const override;

    virtual void waitUntilStopped() override;

    [[nodiscard]] bool startThread();

    void setBehaviorOnThreadException(BehaviorOnThreadException what) {
        behaviorOnThreadException_ = what;
    }

    [[nodiscard]] BehaviorOnThreadException behaviorOnThreadException() const {
        return behaviorOnThreadException_;
    }

protected:
    /*! Inheriting classes must implement a thread loop, in the form:
     *  while (!threadMustQuit()) { }
     */
    virtual void threadMain() = 0;

    [[nodiscard]] bool threadMustQuit() const { return thread_must_quit_ || !allPinsConnectedAndOpen(); }

private:
    static void threadEntryPoint(void* ptr);
    Thread thread_;
    std::thread::id creating_thread_id_;
    bool thread_must_quit_ = false;

    BehaviorOnThreadException behaviorOnThreadException_{BehaviorOnThreadException::STOP_GRAPH_PROPAGATE_EXCEPTION};
};

}  // namespace media_graph

#endif  // MEDIAGRAPH_NODE_H
