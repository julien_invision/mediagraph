// tests that we can compile and link against mediagraph::time
#include <iostream>
#include <mediagraph/timestamp.h>

int main()
{
    Timestamp ts = Timestamp::now();

    std::cout << "Now: " << ts.asString() << std::endl;

    return 0;
}