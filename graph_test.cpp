// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <gtest/gtest.h>
#include <exception>
#include <stdexcept>

#include "graph.h"
#include "node.h"
#include "stream.h"
#include "stream_reader.h"
#include "types/type_definition.h"

namespace media_graph {
namespace {
    class TimestampIncrementerStream : public StreamBase<int> {
    public:
        explicit TimestampIncrementerStream(NodeBase* node)
            : StreamBase<int>("Timestamp incrementer", node) {
            sequence_no_ = 0;
        }
        std::string typeName() const override { return "int"; }
        bool tryRead(StreamReader<int>* reader, int* data, Timestamp* timestamp,
                     SequenceId* seq) override {
            *timestamp = Timestamp::now();
            *data = ++(*reader->lastReadSequenceIdPtr());
            if (seq) { *seq = *data; }
            return true;
        }
        bool read(StreamReader<int>* reader, int* data, Timestamp* timestamp,
                  SequenceId* seq) override {
            while (!(reader->seekPosition() < Timestamp::now())) {}
            return tryRead(reader, data, timestamp, seq);
        }
        bool canRead(SequenceId consumed_until, Timestamp fresher_than) const override {
            return consumed_until < sequence_no_ && fresher_than < Timestamp::now();
        }

    private:
        int sequence_no_;
    };

    class AlwaysEmptyStream : public StreamBase<int> {
    public:
        explicit AlwaysEmptyStream(NodeBase* node) : StreamBase<int>("Always empty", node) {}
        std::string typeName() const override { return "int"; }
        bool tryRead(StreamReader<int>* /*reader*/, int* /*data*/, Timestamp* /*timestamp*/,
                     SequenceId* /*seq*/) override {
            return false;
        }

        bool read(StreamReader<int>* /*reader*/, int* /*data*/, Timestamp* /*timestamp*/,
                  SequenceId* /*seq*/) override {
            return false;
        }

        bool canRead(SequenceId /*consumed_until*/, Timestamp /*fresher_than*/) const override {
            return false;
        }
    };

    class IntProducerNode : public NodeBase {
    public:
        IntProducerNode() : timestamp_incrementer_stream(this), always_empty_stream(this) {}

        int numOutputStream() const override { return 2; }
        const NamedStream* constOutputStream(int index) const override {
            if (index == 0) { return &timestamp_incrementer_stream; }
            if (index == 1) { return &always_empty_stream; }
            return nullptr;
        }

    private:
        TimestampIncrementerStream timestamp_incrementer_stream;
        AlwaysEmptyStream always_empty_stream;
    };

    class IntConsumerNode : public NodeBase {
    public:
        IntConsumerNode() : input("int", this) {}

        int numInputPin() const override { return 1; }
        const NamedPin* constInputPin(int index) const override {
            if (index == 0) { return &input; }
            return nullptr;
        }

        // ensure it's connected to a dummy producer that doesn't produce anything
        void testWaitForPinActivityWithTimeout() {
            const Duration timeout = Duration::milliSeconds(150);

            const Timestamp before = Timestamp::now();
            waitForPinActivity(timeout);
            const Timestamp after = Timestamp::now();

            EXPECT_GE(after - before, timeout);

            // commented out to allow for arbitrary 'kernel come-backs'
            // EXPECT_LT(after - before, timeout + Duration::milliSeconds(2));
        }

        void testSequentialRead(Timestamp after) {
            int value;
            Timestamp timestamp;
            EXPECT_TRUE(input.seek(after));
            EXPECT_TRUE(input.read(&value, &timestamp));
        }

        void testTryRead(Timestamp after) {
            int value;
            Timestamp timestamp;
            EXPECT_TRUE(input.seek(after));
            EXPECT_TRUE(input.tryRead(&value, &timestamp));
        }

        void testReadFrom(Timestamp bound, int num_to_read) {
            EXPECT_TRUE(input.seek(bound));
            Timestamp last_timestamp = bound;
            SequenceId last_sequence_id = -1;
            for (int i = 0; i < num_to_read; ++i) {
                int value;
                Timestamp timestamp;
                SequenceId sequence_id;
                EXPECT_TRUE(input.read(&value, &timestamp, &sequence_id));

                // Make sure the time is monotonic.
                EXPECT_TRUE(!(timestamp < last_timestamp));
                EXPECT_LT(last_sequence_id, sequence_id);
                last_timestamp = timestamp;
                last_sequence_id = sequence_id;
            }
        }

    private:
        StreamReader<int> input;
    };

    class ThreadedIntProducer : public ThreadedNodeBase {
    public:
        explicit ThreadedIntProducer(Duration limit = Duration())
            : output_stream("out", this), time_limit_(limit) {}

        void threadMain() override {
            int sequence_no = 0;
            Timestamp start_time = Timestamp::now();

            while (!threadMustQuit()) {
                // push as fast as we can.
                Timestamp timestamp = Timestamp::now();
                if (time_limit_ != Duration() && (timestamp - start_time) > time_limit_) {
                    std::cout << "Time limit reached, exiting producer thread.\n";
                    return;
                }
                if (!output_stream.update(timestamp, sequence_no)) { break; }
                ++sequence_no;
            }
        }
        int numOutputStream() const override { return 1; }
        const NamedStream* constOutputStream(int index) const override {
            if (index == 0) { return &output_stream; }
            return nullptr;
        }

    private:
        Stream<int> output_stream;
        Duration time_limit_;
    };

    class ThreadedPassThrough : public ThreadedNodeBase {
    public:
        ThreadedPassThrough() : output_stream("out", this), input_stream("in", this) {}

        void threadMain() override {
            while (!threadMustQuit()) {
                int data;
                Timestamp timestamp;
                if (!input_stream.read(&data, &timestamp)) { break; }
                if (!output_stream.update(timestamp, data)) { break; }
            }
        }

        int numInputPin() const override { return 1; }
        const NamedPin* constInputPin(int index) const override {
            return (index == 0 ? &input_stream : nullptr);
        }
        int numOutputStream() const override { return 1; }
        const NamedStream* constOutputStream(int index) const override {
            if (index == 0) { return &output_stream; }
            return nullptr;
        }

    private:
        Stream<int> output_stream;
        StreamReader<int> input_stream;
    };

    class JoinAndCheckEquals : public NodeBase {
    public:
        JoinAndCheckEquals() : input_a("a", this), input_b("b", this) {}

        int numInputPin() const override { return 2; }
        const NamedPin* constInputPin(int i) const override {
            switch (i) {
                case 0: return &input_a;
                case 1: return &input_b;
            }
            return nullptr;
        }

        void testSyncFromA(int num_iterations) {
            for (int i = 0; i < num_iterations; ++i) {
                int value_a;
                Timestamp timestamp_a;
                EXPECT_TRUE(input_a.read(&value_a, &timestamp_a));
                int value_b;
                Timestamp timestamp_b;
                EXPECT_TRUE(input_b.seek(timestamp_a - Duration::microSeconds(1)));
                EXPECT_TRUE(input_b.read(&value_b, &timestamp_b));
                EXPECT_EQ(0, (timestamp_a - timestamp_b).milliSeconds());
                EXPECT_EQ(value_a, value_b);
            }
        }

    private:
        StreamReader<int> input_a;
        StreamReader<int> input_b;
    };

    class NodeWithRepeatedPinNames : public NodeBase
    {
        public:
            NodeWithRepeatedPinNames() : input_a(repeatedName, this), input_b(repeatedName, this)
            {}
            int numInputPin() const override { return 2; }

            const NamedPin* constInputPin(int i) const override {
                switch (i) {
                    case 0: return &input_a;
                    case 1: return &input_b;
                }
                return nullptr;
            }

        private:
            std::string repeatedName = "repeated_pin_name";
            StreamReader<bool> input_a;
            StreamReader<bool> input_b;
    };

    class NodeWithRepeatedStreamNames : public NodeBase
    {
        public:
            NodeWithRepeatedStreamNames() : output_a(repeatedName, this), output_b(repeatedName, this)
            {}

            int numOutputStream() const override { return 2; }
            const NamedStream* constOutputStream(int i) const override {
                switch (i) {
                    case 0: return &output_a;
                    case 1: return &output_b;
                }
                return nullptr;
            }

        private:
            std::string repeatedName = "repeated_stream_name";
            Stream<bool> output_a;
            Stream<bool> output_b;
    };

    class NodeWithPinAndStreamWithSameName : public NodeBase
    {
        public:
            NodeWithPinAndStreamWithSameName() : input(repeatedName, this), output(repeatedName, this)
            {}

            int numInputPin() const override { return 1; }
            const NamedPin* constInputPin(int i) const override {
                switch (i) {
                    case 0: return &input;
                }
                return nullptr;
            }

            int numOutputStream() const override { return 1; }
            const NamedStream* constOutputStream(int i) const override {
                switch (i) {
                    case 0: return &output;
                }
                return nullptr;
            }

        private:
            std::string repeatedName = "repeated_pin_and_stream_name";
            StreamReader<bool> input;
            Stream<bool> output;
    };

    class NodeWithDefaultBehaviorOnException : public ThreadedNodeBase
    {
        // Necessary to be created, but will never be run
        void threadMain() override {};
    };

}  // namespace

// producer -> consumer
TEST(GraphTest, NoThread) {
    Graph graph;

    auto producer = graph.newNode<IntProducerNode>("producer");
    auto consumer = graph.newNode<IntConsumerNode>("consumer");

    EXPECT_TRUE(graph.connect("producer", "Timestamp incrementer", "consumer", "int"));

    // Make sure "connect" does some verifications.
    EXPECT_FALSE(graph.connect("invalid node", "Timestamp incrementer", "consumer", "int"));
    EXPECT_FALSE(graph.connect("producer", "invalid stream", "consumer", "int"));
    EXPECT_FALSE(graph.connect("producer", "Timestamp incrementer", "invalid node", "int"));
    EXPECT_FALSE(graph.connect("producer", "Timestamp incrementer", "consumer", "invalid pin"));

    EXPECT_TRUE(graph.start());

    consumer->testSequentialRead(Timestamp::now() + Duration::milliSeconds(10));
    consumer->testTryRead(Timestamp::now());
}

TEST(GraphTest, waitForPinActivityHonorsTimeout) {
    Graph graph;

    auto producer = graph.newNode<IntProducerNode>("producer");
    auto consumer = graph.newNode<IntConsumerNode>("consumer");
    EXPECT_TRUE(graph.connect("producer", "Always empty", "consumer", "int"));

    EXPECT_TRUE(graph.start());

    consumer->testWaitForPinActivityWithTimeout();
}

// producer -> filter -> consumer
TEST(GraphTest, SimpleThreaded) {
    Graph graph;
    auto producer = graph.newNode<ThreadedIntProducer>("producer");
    auto filter = graph.newNode<ThreadedPassThrough>("filter");
    auto consumer = graph.newNode<IntConsumerNode>("consumer");

    EXPECT_TRUE(graph.connect(producer, "out", filter, "in"));
    EXPECT_TRUE(graph.connect(filter, "out", consumer, "int"));
    EXPECT_TRUE(graph.start());

    consumer->testReadFrom(Timestamp::now(), 100);

    graph.stop();
}

TEST(GraphTest, HotPlug) {
    Graph graph;

    // The graph is started first.
    EXPECT_TRUE(graph.start());

    // We add nodes.
    auto producer = graph.newNode<ThreadedIntProducer>("producer");
    auto filter = graph.newNode<ThreadedPassThrough>("filter");
    auto consumer = graph.newNode<IntConsumerNode>("consumer");

    EXPECT_TRUE(graph.connect(producer, "out", filter, "in"));
    EXPECT_TRUE(graph.connect(filter, "out", consumer, "int"));

    // The nodes have to be started.
    EXPECT_TRUE(producer->start());
    EXPECT_TRUE(filter->start());
    EXPECT_TRUE(consumer->start());

    EXPECT_TRUE(producer->isRunning());
    EXPECT_TRUE(filter->isRunning());
    EXPECT_TRUE(consumer->isRunning());

    // Data should be flowing.
    consumer->testReadFrom(Timestamp::now(), 10);

    // Waow, that's rude.
    graph.removeNode("filter");
    filter.reset();

    EXPECT_FALSE(consumer->isRunning());

    EXPECT_TRUE(graph.connect(producer, "out", consumer, "int"));
    EXPECT_TRUE(consumer->start());

    EXPECT_TRUE(producer->isRunning());
    EXPECT_TRUE(consumer->isRunning());

    consumer->testReadFrom(Timestamp::now(), 10);

    graph.stop();
}

/*
              /--> a --\
             /          \
 producer ->-            ----> consumer
             \          /
              \--> b --/

*/

TEST(GraphTest, JoinSync) {
    Graph graph;
    auto producer = graph.newNode<ThreadedIntProducer>("producer");
    auto filter_a = graph.newNode<ThreadedPassThrough>("a");
    auto filter_b = graph.newNode<ThreadedPassThrough>("b");
    auto consumer = graph.newNode<JoinAndCheckEquals>("consumer");

    EXPECT_EQ(0, producer->getOutputStreamByName("out")->numReaders());
    EXPECT_TRUE(graph.connect(producer, "out", filter_a, "in"));
    EXPECT_EQ(1, producer->getOutputStreamByName("out")->numReaders());
    EXPECT_TRUE(graph.connect("producer", "out", "b", "in"));
    EXPECT_EQ(2, producer->getOutputStreamByName("out")->numReaders());

    EXPECT_TRUE(graph.connect("a", "out", "consumer", "a"));
    EXPECT_TRUE(graph.connect("b", "out", "consumer", "b"));

    EXPECT_TRUE(graph.start());

    consumer->testSyncFromA(10);
    graph.stop();
}

class ThreadedIntConsumer : public ThreadedNodeBase {
public:
    ThreadedIntConsumer() : input_("in", this) {}

    int numInputPin() const override { return 1; }
    const NamedPin* constInputPin(int i) const override {
        switch (i) {
            case 0: return &input_;
        }
        return nullptr;
    }

    void threadMain() override {
        while (!threadMustQuit()) {
            int value;
            Timestamp timestamp;

            // Here, we "forget" to check the return value of input_, on purpose.
            // That is to make sure threadMustQuit() returns true when at least one input
            // becomes invalid.
            input_.read(&value, &timestamp);

            if (throwException_) {
                throwException_ = false;  // signal back
                throw std::runtime_error("I am throwing this exception because you asked me to");
            }
        }
    }

    void waitForNextInputAndThrowException() {
        throwException_ = true;

        // polling is not the cleanest option, but we want to keep the code simple
        while (throwException_) { std::this_thread::sleep_for(std::chrono::milliseconds(1)); }
    }

private:
    StreamReader<int> input_;
    bool throwException_{false};
};

TEST(GraphTest, shouldNoticeWhenStopped) {
    Graph graph;
    auto producer = graph.newNode<ThreadedIntProducer>("producer", Duration::milliSeconds(50));
    auto consumer = graph.newNode<ThreadedIntConsumer>("consumer");

    EXPECT_TRUE(graph.connect(producer, "out", consumer, "in"));
    EXPECT_TRUE(graph.start());

    EXPECT_TRUE(graph.isStarted());
    EXPECT_TRUE(graph.atLeastOneNodeRunning());
    graph.waitUntilStopped();
    EXPECT_FALSE(graph.atLeastOneNodeRunning());
}

TEST(GraphTest, onlyRenameAndAddNamedNodeIfOrphanNodeOrNameNotInGraph) {
    Graph graph;

    auto producer = std::make_shared<ThreadedIntProducer>(Duration::milliSeconds(1));

    std::set<Graph*> set_with_one_graph{&graph};

    EXPECT_TRUE(producer->setName("dummy_name"));
    EXPECT_TRUE(graph.addNode("producer", producer));
    EXPECT_EQ(graph.numNodes(), 1);
    EXPECT_TRUE(graph.getNodeByName("producer"));
    EXPECT_FALSE(graph.getNodeByName("dummy_name"));
    EXPECT_EQ(producer->name(), "producer");
    EXPECT_EQ(producer->graphs(), set_with_one_graph);

    auto producer2 = std::make_shared<ThreadedIntProducer>(Duration::milliSeconds(1));
    EXPECT_TRUE(producer2->setName("producer"));
    EXPECT_FALSE(graph.addNode("producer", producer2));
    EXPECT_EQ(graph.numNodes(), 1);
    EXPECT_EQ(producer->graphs(), set_with_one_graph);
    EXPECT_TRUE(producer2->graphs().empty());
}

TEST(GraphTest, onlyAddNodeWithoutRenameIfNodeIsNamedAndNameNotInGraph) {
    Graph graph;

    auto producer = std::make_shared<ThreadedIntProducer>(Duration::milliSeconds(1));

    EXPECT_FALSE(producer->isNamed());
    EXPECT_FALSE(graph.addNode(producer));
    EXPECT_EQ(graph.numNodes(), 0);
    EXPECT_TRUE(producer->graphs().empty());

    std::set<Graph*> set_with_one_graph{&graph};

    EXPECT_TRUE(producer->setName("producer"));
    EXPECT_TRUE(producer->isNamed());
    EXPECT_TRUE(graph.addNode(producer));
    EXPECT_TRUE(graph.getNodeByName("producer"));
    EXPECT_EQ(graph.numNodes(), 1);
    EXPECT_EQ(producer->graphs(), set_with_one_graph);

    auto producer_2 = std::make_shared<ThreadedIntProducer>(Duration::milliSeconds(1));

    EXPECT_TRUE(producer_2->setName("producer"));
    EXPECT_FALSE(graph.addNode(producer_2));
    EXPECT_TRUE(graph.getNodeByName("producer"));
    EXPECT_EQ(graph.numNodes(), 1);
    EXPECT_EQ(producer->graphs(), set_with_one_graph);

    EXPECT_TRUE(producer_2->graphs().empty());
}

TEST(GraphTest, connectAddsNodesToGraph) {
    Graph graph_1;
    Graph graph_2;
    Graph graph_3;

    auto producer = graph_1.newNode<ThreadedIntProducer>("producer");
    auto consumer = graph_2.newNode<ThreadedIntConsumer>("consumer");

    EXPECT_TRUE(graph_3.connect(producer, "out", consumer, "in"));
    EXPECT_TRUE(graph_3.getNodeByName("producer"));
    EXPECT_TRUE(graph_3.getNodeByName("consumer"));

    std::set<Graph*> set_with_graphs_1_and_3{&graph_1, &graph_3};
    EXPECT_EQ(producer->graphs(), set_with_graphs_1_and_3);

    std::set<Graph*> set_with_graphs_2_and_3{&graph_2, &graph_3};
    EXPECT_EQ(consumer->graphs(), set_with_graphs_2_and_3);
}

TEST(GraphTest, connectDoesNotAddNodesOnFailure) {
    Graph graph;

    auto addable_producer = std::make_shared<ThreadedIntProducer>(Duration::milliSeconds(1));
    EXPECT_TRUE(addable_producer->setName("producer"));
    auto addable_consumer = std::make_shared<ThreadedIntConsumer>();
    EXPECT_TRUE(addable_consumer->setName("consumer"));

    auto non_addable_producer = std::make_shared<ThreadedIntProducer>(Duration::milliSeconds(1));
    auto non_addable_consumer = std::make_shared<ThreadedIntConsumer>();
    
    // Failure mode: Cannot add unnamed nodes
    EXPECT_FALSE(graph.connect(addable_producer, "out", non_addable_consumer, "in"));
    EXPECT_EQ(graph.numNodes(), 0);
    EXPECT_TRUE(addable_producer->graphs().empty());
    EXPECT_TRUE(non_addable_consumer->graphs().empty());
    
    EXPECT_FALSE(graph.connect(non_addable_producer, "out", addable_consumer, "in"));
    EXPECT_EQ(graph.numNodes(), 0);
    EXPECT_TRUE(non_addable_producer->graphs().empty());
    EXPECT_TRUE(addable_consumer->graphs().empty());
    
    std::set set_with_graph{&graph};

    // Failure mode: Name conflict
    EXPECT_TRUE(graph.connect(addable_producer, "out", addable_consumer, "in"));
    EXPECT_EQ(graph.numNodes(), 2);
    EXPECT_TRUE(non_addable_producer->setName("producer"));
    EXPECT_TRUE(non_addable_consumer->setName("consumer"));
    
    EXPECT_FALSE(graph.connect(addable_producer, "out", non_addable_consumer, "in"));
    EXPECT_EQ(graph.numNodes(), 2);
    EXPECT_EQ(addable_producer->graphs(), set_with_graph);
    EXPECT_TRUE(non_addable_consumer->graphs().empty());

    EXPECT_FALSE(graph.connect(non_addable_producer, "out", addable_consumer, "in"));
    EXPECT_EQ(graph.numNodes(), 2);
    EXPECT_TRUE(non_addable_producer->graphs().empty());
    EXPECT_EQ(addable_consumer->graphs(), set_with_graph);
}

TEST(GraphTest, graphsDoNotContainDetachedNodes) {
    Graph graph_1;
    Graph graph_2;
    Graph graph_3;

    auto producer = graph_1.newNode<ThreadedIntProducer>("producer");
    EXPECT_TRUE(graph_2.addNode(producer));
    EXPECT_TRUE(graph_3.addNode(producer));

    producer->detach(&graph_1);
    EXPECT_EQ(graph_1.numNodes(), 0);
    EXPECT_EQ(graph_2.numNodes(), 1);
    EXPECT_EQ(graph_3.numNodes(), 1);
    EXPECT_EQ(producer->name(), "producer");

    std::set<Graph*> graphs_2_and_3{&graph_2, &graph_3};
    EXPECT_EQ(producer->graphs(), graphs_2_and_3);

    producer->detach();
    EXPECT_EQ(graph_2.numNodes(), 0);
    EXPECT_EQ(graph_3.numNodes(), 0);
    EXPECT_TRUE(producer->isNamed());
    EXPECT_EQ(producer->graphs().size(), 0);

    // Check calling detach(Graph*) with the last Graph* the node belongs to. 
    EXPECT_TRUE(graph_1.addNode(producer));
    producer->detach(&graph_1);
    EXPECT_EQ(graph_1.numNodes(), 0);
    EXPECT_TRUE(producer->isNamed());
    EXPECT_EQ(producer->graphs().size(), 0);
}

TEST(GraphTest, clearingGraphDetachesNodes) {
    Graph graph_1;
    Graph graph_2;

    auto producer = graph_1.newNode<ThreadedIntProducer>("producer");
    EXPECT_TRUE(graph_2.addNode(producer));

    graph_2.clear();
    EXPECT_EQ(graph_1.numNodes(), 1);
    EXPECT_EQ(graph_2.numNodes(), 0);
    EXPECT_EQ(producer->name(), "producer");
    
    EXPECT_EQ(producer->graphs(), std::set<Graph*>{&graph_1});

    graph_1.clear();
    EXPECT_EQ(graph_1.numNodes(), 0);
    EXPECT_TRUE(producer->isNamed());
    EXPECT_EQ(producer->graphs(), std::set<Graph*>{});
}

TEST(GraphTest, graphDestructorDetachesNodes) {
    auto* graph_1 = new Graph();
    auto* graph_2 = new Graph();

    auto producer = graph_1->newNode<ThreadedIntProducer>("producer");
    EXPECT_TRUE(graph_2->addNode(producer));

    delete graph_2;
    EXPECT_EQ(graph_1->numNodes(), 1);
    EXPECT_EQ(producer->name(), "producer");
    EXPECT_EQ(producer->graphs(), std::set<Graph*>{graph_1});

    delete  graph_1;
    EXPECT_TRUE(producer->isNamed());
    EXPECT_EQ(producer->graphs(), std::set<Graph*>{});
}

TEST(ThreadedNodeTest, defaultStopBehaviorIsStopGraphAndPropagateException) {
    Graph graph;
    auto producer = graph.newNode<NodeWithDefaultBehaviorOnException>("nodeWithDefaultExceptionBehavior");
    EXPECT_EQ(producer->behaviorOnThreadException(),
              ThreadedNodeBase::BehaviorOnThreadException::STOP_GRAPH_PROPAGATE_EXCEPTION);
}

TEST(GraphTest, stopsGraphIfConsumerThrowsExceptionAndBehaviorSetToStopGraph) {
    Graph graph;
    auto producer = graph.newNode<ThreadedIntProducer>("producer");

    auto consumer = graph.newNode<ThreadedIntConsumer>("consumer_that_throws");
    consumer->setBehaviorOnThreadException(ThreadedNodeBase::BehaviorOnThreadException::STOP_GRAPH);

    EXPECT_TRUE(graph.connect(producer, "out", consumer, "in"));
    EXPECT_TRUE(graph.start());

    EXPECT_TRUE(graph.isStarted());
    EXPECT_TRUE(graph.atLeastOneNodeRunning());

    consumer->waitForNextInputAndThrowException();
    graph.waitUntilStopped();  // if behavior == NORMAL_NODE_STOP -> it wouldn't ever stop

    EXPECT_FALSE(graph.atLeastOneNodeRunning());
}

TEST(GraphTest, stopsAllAttachedGraphsIfConsumerThrowsExceptionAndBehaviorSetToStopGraph) {
    Graph graph_1;
    Graph graph_2;
    Graph graph_3;

    auto producer = graph_1.newNode<ThreadedIntProducer>("producer");

    auto consumer = graph_2.newNode<ThreadedIntConsumer>("consumer_that_throws");
    consumer->setBehaviorOnThreadException(ThreadedNodeBase::BehaviorOnThreadException::STOP_GRAPH);

    EXPECT_TRUE(graph_3.addNode(consumer));

    EXPECT_TRUE(graph_2.connect(producer, "out", consumer, "in"));
    EXPECT_TRUE(graph_2.start());

    EXPECT_TRUE(graph_2.isStarted());
    EXPECT_TRUE(graph_2.atLeastOneNodeRunning());

    consumer->waitForNextInputAndThrowException();
    graph_2.waitUntilStopped();  // if behavior == NORMAL_NODE_STOP -> it wouldn't ever stop

    EXPECT_FALSE(graph_1.atLeastOneNodeRunning());
    EXPECT_FALSE(graph_2.atLeastOneNodeRunning());
    EXPECT_FALSE(graph_3.atLeastOneNodeRunning());
}

TEST(GraphTest, stopsAllAttachedGraphsAndThrowsExceptionIfConsumerThrowsExceptionAndBehaviorSetToStopGraphAndPropagateException) {
    Graph graph_1;
    Graph graph_2;
    Graph graph_3;

    auto producer = graph_1.newNode<ThreadedIntProducer>("producer");

    auto consumer = graph_2.newNode<ThreadedIntConsumer>("consumer_that_throws");
    consumer->setBehaviorOnThreadException(ThreadedNodeBase::BehaviorOnThreadException::STOP_GRAPH_PROPAGATE_EXCEPTION);

    EXPECT_TRUE(graph_3.addNode(consumer));

    EXPECT_TRUE(graph_2.connect(producer, "out", consumer, "in"));
    
    std::exception_ptr maybeException;
    try {
        EXPECT_TRUE(graph_2.start());
        consumer->waitForNextInputAndThrowException();
        graph_2.waitUntilStopped();  // if behavior == NORMAL_NODE_STOP -> it wouldn't ever stop
    } 
    catch(std::runtime_error& e)
    {
        // Ensure this is the exception we expect
        EXPECT_EQ(std::string(e.what()), "I am throwing this exception because you asked me to");
        maybeException = std::current_exception();
    }
    EXPECT_NE(maybeException, nullptr) << "Node exception was not propagated";

    EXPECT_FALSE(graph_1.atLeastOneNodeRunning());
    EXPECT_FALSE(graph_2.atLeastOneNodeRunning());
    EXPECT_FALSE(graph_3.atLeastOneNodeRunning());
}

TEST(GraphTest, DoesNotStopGraphIfConsumerThrowsExceptionAndBehaviorNotSetToStopGraph) {
    Graph graph;
    auto producer = graph.newNode<ThreadedIntProducer>("producer");

    auto consumer = graph.newNode<ThreadedIntConsumer>("consumer_that_throws");
    consumer->setBehaviorOnThreadException(
        ThreadedNodeBase::BehaviorOnThreadException::NORMAL_NODE_STOP);

    EXPECT_TRUE(graph.connect(producer, "out", consumer, "in"));
    EXPECT_TRUE(graph.start());

    EXPECT_TRUE(graph.isStarted());
    EXPECT_TRUE(graph.atLeastOneNodeRunning());
    consumer->waitForNextInputAndThrowException();

    std::this_thread::sleep_for(std::chrono::milliseconds(10));  // let's give it some slack time
    EXPECT_TRUE(graph.atLeastOneNodeRunning());                              // should still be running

    graph.stop();
}

TEST(GraphTest, CanConnectNodesFromDifferentGraphs) {
    Graph graph_1;

    auto graph_1_producer = graph_1.newNode<ThreadedIntProducer>("graph_1_producer");
    auto graph_1_consumer = graph_1.newNode<ThreadedIntConsumer>("graph_1_consumer");

    Graph graph_2;
    auto graph_2_producer = graph_2.newNode<ThreadedIntProducer>("graph_2_producer");
    auto graph_2_consumer = graph_2.newNode<ThreadedIntConsumer>("graph_2_consumer");

    EXPECT_TRUE(graph_1.connect(graph_1_producer, "out", graph_2_consumer, "in"));
    EXPECT_TRUE(graph_1.connect(graph_2_producer, "out", graph_1_consumer, "in"));
}

TEST(GraphTest, streamCanSendTimestampZeroMessageAndReaderCanRead)
{
    StreamReader<int> myReader("myreader", nullptr);
    Stream<int> myStream("mystream", nullptr, media_graph::WAIT_FOR_CONSUMPTION_NEVER_DROP, 4);
    myReader.connect(&myStream);

    EXPECT_TRUE(myStream.update(Timestamp::microSecondsSince1970(0), 12));
    EXPECT_TRUE(myStream.update(Timestamp::microSecondsSince1970(1), 13));

    // no one read them so they're still in the queue
    EXPECT_EQ(myStream.numItemsInQueue(), 2);
    EXPECT_EQ(myStream.numDroppedItems(), 0);

    // read both
    {
        int readData; Timestamp readTimestamp;
        const bool readOk = myReader.read(&readData, &readTimestamp);
        ASSERT_TRUE(readOk);
        EXPECT_EQ(readTimestamp, Timestamp::microSecondsSince1970(0));
        EXPECT_EQ(readData, 12);
    }

    {
        int readData; Timestamp readTimestamp;
        const bool readOk = myReader.read(&readData, &readTimestamp);
        ASSERT_TRUE(readOk);
        EXPECT_EQ(readTimestamp, Timestamp::microSecondsSince1970(1));
        EXPECT_EQ(readData, 13);
    }
}

TEST(GraphTest, streamNumDroppedAndNumInQueuePropertiesReflectActualValuesWithNeverBlockDropOldest) {

    ThreadedIntConsumer dummyNode;

    StreamReader<int> myReader("myreader", nullptr);
    Stream<int> myStream("mystream", nullptr, media_graph::NEVER_BLOCK_DROP_OLDEST, 4);
    myReader.connect(&myStream);

    EXPECT_EQ(myStream.numItemsInQueue(), 0);
    EXPECT_EQ(myStream.numDroppedItems(), 0);

    // add 2 items
    myStream.update(Timestamp::now(), -2);
    myStream.update(Timestamp::now(), 1);

    // no one read them so they're still in the queue
    EXPECT_EQ(myStream.numItemsInQueue(), 2);
    EXPECT_EQ(myStream.numDroppedItems(), 0);

    // read one
    {
        int readData; Timestamp readTimestamp;
        const bool readOk = myReader.read(&readData, &readTimestamp);
        ASSERT_TRUE(readOk);
        EXPECT_EQ(readData, -2);
    }

    EXPECT_EQ(myStream.numItemsInQueue(), 1);
    EXPECT_EQ(myStream.numDroppedItems(), 0);

    // now add more than available in message queue
    myStream.update(Timestamp::now(), 2);
    myStream.update(Timestamp::now(), 3);
    myStream.update(Timestamp::now(), 4);
    myStream.update(Timestamp::now(), 5);
    myStream.update(Timestamp::now(), 6);

    EXPECT_EQ(myStream.numItemsInQueue(), 4);
    EXPECT_EQ(myStream.numDroppedItems(), 2);

    // and next ones to pop must be the ones we want
    for (int i=3; i < 7; ++i) {
        int readData; Timestamp readTimestamp;
        const bool readOk = myReader.read(&readData, &readTimestamp);
        ASSERT_TRUE(readOk);
        EXPECT_EQ(readData, i);
    }

    EXPECT_EQ(myStream.numItemsInQueue(), 0);
    EXPECT_EQ(myStream.numDroppedItems(), 2);
}

TEST(GraphTest, doNotAddNodeWithRepeatedPinOrStreamNames) {

    Graph graph;

    auto repeatedPinNames = std::make_shared<NodeWithRepeatedPinNames>();
    auto repeatedStreamNames = std::make_shared<NodeWithRepeatedStreamNames>();

    // Test the Graph::addNode(name, node) function 
    EXPECT_FALSE(graph.addNode("repeatedPinNames", repeatedPinNames));
    EXPECT_FALSE(graph.addNode("repeatedStreamNames", repeatedStreamNames));

    // Test the Graph::addNode(node) function
    EXPECT_TRUE(repeatedPinNames->setName("repeatedPinNames"));
    EXPECT_FALSE(graph.addNode(repeatedPinNames));

    EXPECT_TRUE(repeatedStreamNames->setName("repeatedStreamNames"));
    EXPECT_FALSE(graph.addNode(repeatedStreamNames));
}

TEST(GraphTest, canAddNodeWithPinWithSameNameAsStream) {
    
    // Checks that the logic for preventing repeated pin names and duplicate
    // repeated stream names isn't preventing pins from being named the same as
    // streams

    Graph graph;

    auto pinAndStreamWithSameName = std::make_shared<NodeWithPinAndStreamWithSameName>();

    // Test the Graph::addNode(name, node) function 
    EXPECT_TRUE(graph.addNode("pinAndStreamWithSameName", pinAndStreamWithSameName));
    pinAndStreamWithSameName->detach();

    // Test the Graph::addNode(node) function
    EXPECT_TRUE(pinAndStreamWithSameName->setName("pinAndStreamWithSameName"));
    EXPECT_TRUE(graph.addNode(pinAndStreamWithSameName));
}

}  // namespace media_graph
