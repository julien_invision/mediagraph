#ifndef MEDIAGRAPH_DURATION_H
#define MEDIAGRAPH_DURATION_H

#include <stdint.h>
#include <chrono>
#include <string>

class Timestamp;

//! Represents a relative time period.
//! A duration can be obtained by:
//!  - specifying a constant duration in a specified unit;
//!  - subtracting two Timestamp objects.
//!
//! The default constructor gives a duration or length 0.
class Duration {
public:
    Duration() { duration_ = 0; }

    // Conversion from/to std::microseconds is transparent
    // cppcheck-suppress noExplicitConstructor
    Duration(std::chrono::microseconds t) : duration_(t.count()) {}
    operator std::chrono::microseconds() const { return std::chrono::microseconds(duration_); }

    static Duration seconds(double sec) { return Duration(int64_t(sec * 1e6)); }
    static Duration milliSeconds(double msec) { return Duration(int64_t(msec * 1e3)); }
    static Duration microSeconds(int64_t microsec) { return Duration(microsec); }

    [[nodiscard]] int64_t microSeconds() const { return duration_; }
    [[nodiscard]] int64_t milliSeconds() const { return duration_ / 1000; }
    [[nodiscard]] double seconds() const { return double(duration_) * 1e-6; }

    //! Pause execution of the current thread for the specified duration.
    //! The caller is guaranteed to be stopped for at least the duration,
    //! but it could be more. Expect a few milliseconds.
    //! To wait for a short and more acurate time, call Timestamp::now()
    //! until it reaches the time you want.
    void sleep() const;

    [[nodiscard]] Duration abs() const { return Duration(duration_ > 0 ? duration_ : -duration_); }
    [[nodiscard]] bool isPositive() const { return duration_ > 0; }

    [[nodiscard]] Duration operator+(Duration a) const { return Duration(duration_ + a.duration_); }

    [[nodiscard]] Duration operator-(Duration a) const { return Duration(duration_ - a.duration_); }

    [[nodiscard]] Duration operator*(double factor) const {
        return Duration(static_cast<int64_t>(duration_ * factor));
    }

    [[nodiscard]] Duration operator*(int64_t factor) const { return Duration(duration_ * factor); }

    [[nodiscard]] bool operator<(Duration a) const { return duration_ < a.duration_; }
    [[nodiscard]] bool operator>(Duration a) const { return duration_ > a.duration_; }
    [[nodiscard]] bool operator<=(Duration a) const { return duration_ <= a.duration_; }
    [[nodiscard]] bool operator>=(Duration a) const { return duration_ >= a.duration_; }
    [[nodiscard]] bool operator==(Duration a) const { return duration_ == a.duration_; }
    [[nodiscard]] bool operator!=(Duration a) const { return duration_ != a.duration_; }

    [[nodiscard]] std::string format() const;

private:
    explicit Duration(int64_t microsec) : duration_(microsec) {}

    int64_t duration_;

    friend class Timestamp;
};

#endif
