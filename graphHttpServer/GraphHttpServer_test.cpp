// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "../graphHttpServer/GraphHttpServer.h"
#include "../graph.h"
#include "../node.h"
#include "../stream.h"
#include "../stream_reader.h"
#include "../timestamp.h"
#include "../types/type_definition.h"

#include <iostream>

namespace media_graph {
class ThreadedIntProducer : public ThreadedNodeBase {
public:
    ThreadedIntProducer()
        : output_stream("out", this),
          delayProperty(this, "delay", &ThreadedIntProducer::getDelay, &ThreadedIntProducer::setDelay),
          delay(Duration::milliSeconds(10)),
          increase(this, "increase", true) { }
    void threadMain() override {
        Timestamp timestamp;
        int sequence = 0;
        while (!threadMustQuit()) {
            // push at a regular pace
            if (!output_stream.update(timestamp, sequence)) { break; }
            if (increase) { sequence++; } else {sequence--; }
            delay.sleep();
        }
    }
    int numOutputStream() const override { return 1; }
    const NamedStream* constOutputStream(int index) const override {
        if (index == 0) { return &output_stream; }
        return nullptr;
    }

    int64_t getDelay() const { return delay.milliSeconds(); }
    bool setDelay(const int64_t& ms) {
        if (ms < 0) { return false; }
        delay = Duration::milliSeconds(ms);
        return true;
    }
    
private:
    Stream<int> output_stream;
    GetSetProperty<int64_t, ThreadedIntProducer> delayProperty;
    Duration delay;
    Property<bool> increase;
};

class ThreadedPassThrough : public ThreadedNodeBase {
public:
    ThreadedPassThrough() : output_stream("out", this), input_stream("in", this) {}

    void threadMain() override {
        while (!threadMustQuit()) {
            int data;
            Timestamp timestamp;
            if (!input_stream.read(&data, &timestamp)) { break; }
            if (!output_stream.update(timestamp, data)) { break; }
        }
    }

    int numInputPin() const override { return 1; }
    const NamedPin* constInputPin(int index) const override {
        return (index == 0 ? &input_stream : nullptr);
    }
    int numOutputStream() const override { return 1; }
    const NamedStream* constOutputStream(int index) const override {
        if (index == 0) { return &output_stream; }
        return nullptr;
    }

private:
    Stream<int> output_stream;
    StreamReader<int> input_stream;
};

void ConstructGraph(Graph* graph) {
    auto producer = graph->newNode<ThreadedIntProducer>("producer");
    auto passThrough = graph->newNode<ThreadedPassThrough>("passthrough");
    if(!Graph::connect(producer->outputStream(0), passThrough->inputPin(0))) {
        std::cout << "Could not connect graph" << std::endl;
        std::abort();
    }

    if (!graph->start()) {
        std::cout << "Could not start graph" << std::endl;
        std::abort();
    }
}

}  // namespace media_graph

int main() {
    media_graph::Graph graph;
    media_graph::ConstructGraph(&graph);

    media_graph::GraphHttpServer server(&graph, 1212);
    std::cout << "http://localhost:1212/\n";

    while (true) { Duration::milliSeconds(100).sleep(); }

    return 0;
}
