// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#include "GraphHttpServer.h"

#include <iomanip>
#include <sstream>
#include <civetweb.h>

#include "SplitString.h"
#include "../graph.h"
#include "../property.h"
#include "../stream.h"
#include "../stream_reader.h"

namespace media_graph {
namespace {
    // see https://stackoverflow.com/questions/7724448/simple-json-string-escape-for-c
    std::string EscapeJson(const std::string& s) {
        std::ostringstream o;
        o << '"';
        for (unsigned char c : s) {
            switch (c) {
                case '"': o << "\\\""; break;
                case '\\': o << "\\\\"; break;
                case '\b': o << "\\b"; break;
                case '\f': o << "\\f"; break;
                case '\n': o << "\\n"; break;
                case '\r': o << "\\r"; break;
                case '\t': o << "\\t"; break;
                default:
                    if (c <= 0x1f) {
                        o << "\\u" << std::hex << std::setw(4) << std::setfill('0')
                          << static_cast<int>(c);
                    } else {
                        o << c;
                    }
            }
        }
        o << '"';
        return o.str();
    }

    class ToJsonValue : public TypeConstVisitor {
    public:
        bool process(const int& value) override {
            result_ = std::to_string(value);
            return true;
        }
        bool process(const int64_t& value) override {
            result_ = std::to_string(value);
            return true;
        }
        bool process(const bool& value) override {
            result_ = (value ? "true" : "false");
            return true;
        }
        bool process(const float& value) override {
            result_ = std::to_string(value);
            return true;
        }
        bool process(const double& value) override {
            result_ = std::to_string(value);
            return true;
        }
        bool process(const std::string& value) override {
            result_ = EscapeJson(value);
            return true;
        }

        [[nodiscard]] const std::string& json() const { return result_; }

    private:
        std::string result_;
    };

    void ListNodes(Graph* graph, HttpReply* reply) {
        reply->text += '[';
        for (int i = 0; i < graph->numNodes(); ++i) {
            reply->text += '"' + graph->node(i)->name() + '"';
            if (i + 1 < graph->numNodes()) { reply->text += ","; }
        }
        reply->text += ']';
    }

    void ListProperties(PropertyList* list, HttpReply* reply) {
        if (!list) {
            reply->setNotFound();
            return;
        }

        std::ostringstream ss;
        ss << "[";
        for (int i = 0; i < list->numProperty(); ++i) {
            const NamedProperty* property = list->property(i);
            ss << "{name:" << EscapeJson(property->name())
               << ",type:" << EscapeJson(property->typeName());
            if (property->isWritable()) { ss << ",writable: true"; }
            ss << ",value:" << EscapeJson(property->ValueToString()) << "}";

            if (i + 1 < list->numProperty()) { ss << ","; }
        }
        ss << "]";

        reply->text += ss.str();
    }

    void ServeNode(NodeBase* node, HttpReply* reply) {
        if (!node) {
            reply->setNotFound();
            return;
        }

        std::ostringstream ss;
        ss << "{name:" << EscapeJson(node->name()) << ",output:[";

        for (int i = 0; i < node->numOutputStream(); ++i) {
            NamedStream* stream = node->outputStream(i);
            ss << "{name:" << EscapeJson(stream->streamName())
               << ",type:" << EscapeJson(stream->typeName()) << "}";
            if (i + 1 < node->numOutputStream()) { ss << ','; }
        }
        ss << "],input:[";

        for (int i = 0; i < node->numInputPin(); ++i) {
            NamedPin* pin = node->inputPin(i);
            ss << "{name:" << EscapeJson(pin->name()) << ",type:" << EscapeJson(pin->typeName());
            if (pin->isConnected()) {
                ss << ",connection:{node:" << EscapeJson(pin->connectedStream()->node()->name())
                   << ",stream:" << EscapeJson(pin->connectedStream()->streamName()) << "}";
            }
            ss << "}";
            if (i + 1 < node->numInputPin()) { ss << ','; }
        }
        ss << "]}";
        reply->text += ss.str();
    }

    // Handle URI with prefix /node/nodeName, assuming nodeName is valid.
    void ServeValidNodeDir(const std::vector<std::string>& directories, NodeBase* node,
                           HttpReply* reply) {
        if (directories.size() == 2) {
            // /node/<node name>, we list pins and streams.
            ServeNode(node, reply);
        } else {
            if (directories[2] == "props") {
                // /node/<node name>/props, we list node properties.
                ListProperties(node, reply);
            } else if (directories[2] == "stream") {
                if (directories.size() >= 4) {
                    // /node/<node name>/stream/<stream name>
                    // we list stream properties
                    ListProperties(node->getOutputStreamByName(directories[3]), reply);
                } else {
                    reply->text = EscapeJson("No stream name given");
                    reply->setNotFound();
                }
            } else if (directories[2] == "pin") {
                if (directories.size() >= 4) {
                    // /node/<node name>/pin/<pin name>
                    // we list pin properties.
                    ListProperties(node->getInputPinByName(directories[3]), reply);
                } else {
                    reply->text = EscapeJson("No pin name given");
                    reply->setNotFound();
                }
            } else {
                reply->setNotFound();
            }
        }
    }

    // Handle URI with prefix: /node
    void ServeNodeDir(Graph* graph, HttpReply* reply) {
        std::string uri = reply->getUri();
        std::vector<std::string> directories = SplitString(uri, "/");

        if (directories.size() >= 2) {
            // We have /node/<node name>/...
            std::shared_ptr<NodeBase> node = graph->getNodeByName(directories[1]);
            if (node) {
                ServeValidNodeDir(directories, node.get(), reply);
            } else {
                reply->text += EscapeJson("Node not found.");
                reply->setNotFound();
            }
        } else {
            reply->text = EscapeJson("Node not specified");
            reply->setNotFound();
        }
    }

    void findPropertyAndHandlePost(PropertyList* obj, const std::string& name, HttpReply* reply) {
        NamedProperty* maybeProp = obj->getPropertyByName(name);
        if (maybeProp) {
            std::string data = reply->getPostData();

            if (maybeProp->ValueFromString(data)) {
                reply->setOK();
            } else {
                reply->text = EscapeJson("Bad value");
                reply->setBadRequest();
            }
        } else {
            reply->text = EscapeJson("Property not found.");
            reply->setNotFound();
        }
    }

    void HandlePostRequest(Graph* graph, HttpReply* reply) {
        std::string uri = reply->getUri();
        std::vector<std::string> directories = SplitString(uri, "/");

        // Do we have a graph property?
        if (directories.size() == 2 && directories[0] == "props") {
            findPropertyAndHandlePost(graph, directories[1], reply);
        } else if (directories.size() >= 2 && directories[0] == "node") {
            std::shared_ptr<NodeBase> node = graph->getNodeByName(directories[1]);
            if (node) {
                // Do we have /node/<node name>/props/<prop name> ?
                if (directories.size() == 4 && directories[2] == "props") {
                    findPropertyAndHandlePost(node.get(), directories[3], reply);
                }
                // Do we have /node/<node name>/pin/<pin>/props/<prop name> ?
                else if (directories.size() == 6 && directories[2] == "pin" &&
                           directories[4] == "props") {
                    NamedPin* pin = node->getInputPinByName(directories[3]);
                    if (pin) {
                        findPropertyAndHandlePost(pin, directories[5], reply);
                    } else {
                        reply->text = EscapeJson("pin not found");
                    }
                }
                // Do we have /node/<node name>/stream/<stream>/props/<prop name> ?
                else if (directories.size() == 6 && directories[0] == "node" &&
                           directories[2] == "stream" && directories[4] == "props") {
                    NamedStream* stream = node->getOutputStreamByName(directories[3]);
                    if (stream) {
                        findPropertyAndHandlePost(stream, directories[5], reply);
                    } else {
                        reply->text = EscapeJson("stream not found");
                        reply->setNotFound();
                    }
                } else {
                    reply->text = EscapeJson("wrong path");
                    reply->setNotFound();
                }
            } else {
                reply->text = EscapeJson("Node not found.");
                reply->setNotFound();
            }
        } else {
            reply->text = EscapeJson("Node not specified");
            reply->setNotFound();
        }
    }

    void Finalize(HttpReply* reply) {
        reply->text += "\r\n";
        reply->setAjaxContent();
        reply->handleJsonp();
        reply->send();
    }

}  // namespace

GraphHttpServer::GraphHttpServer(Graph* graph, int port, const std::string& public_directory)
    : HttpServer(port, public_directory), graph_(graph) {
    setHandler(HttpServer::GET, "/props", [this](std::unique_ptr<HttpReply> reply) {
        ListProperties(graph_, reply.get());
        Finalize(reply.get());
        return true;
    });

    setHandler(HttpServer::GET, "/node", [this](std::unique_ptr<HttpReply> reply) {
        ServeNodeDir(graph_, reply.get());
        Finalize(reply.get());
        return true;
    });

    auto post = [this](std::unique_ptr<HttpReply> reply) {
        HandlePostRequest(graph_, reply.get());
        Finalize(reply.get());
        return true;
    };
    setHandler(HttpServer::POST, "/node", post);
    setHandler(HttpServer::POST, "/props", post);

    setHandler(HttpServer::GET, "/nodeList", [this](std::unique_ptr<HttpReply> reply) {
        ListNodes(graph_, reply.get());
        Finalize(reply.get());
        return true;
    });
}

}  // namespace media_graph
