// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#ifndef MEDIAGRAPH_TIMESTAMP_H
#define MEDIAGRAPH_TIMESTAMP_H

#include <stdint.h>
#include <chrono>
#include <optional>
#include <string>
#include <iosfwd>

#include "duration.h"

//! Represent the time and date at which an event occured.
//!
//! The internal accuracy varies from a system to another.
//! The unit test checks that it is at least 100 micro-sec.
//! The internal unit is microseconds.
class Timestamp {
public:
    //! Create a timestamp containing the creation time.
    Timestamp() { *this = now(); }

    //! Returns a timestamp containing the current time.
    [[nodiscard]] static Timestamp now();

    [[nodiscard]] static Timestamp microSecondsSince1970(int64_t epoch) { return Timestamp(epoch); }
    [[nodiscard]] static Timestamp milliSecondsSince1970(int64_t epochMsec) { return Timestamp(epochMsec * 1000); }
    [[nodiscard]] static Timestamp secondsSince1970(double epochSec) { return Timestamp(static_cast<int64_t>(epochSec * 1e6)); }

    [[nodiscard]] static Timestamp oldestPossible() { return Timestamp(std::numeric_limits<int64_t>::min()); }
    [[nodiscard]] static Timestamp largestPossible() { return Timestamp(std::numeric_limits<int64_t>::max()); }

    [[nodiscard]] int64_t microSecondsSince1970() const { return epoch_; }
    [[nodiscard]] double milliSecondsSince1970() const { return epoch_ / 1e3; }
    [[nodiscard]] double secondsSince1970() const { return epoch_ / 1e6; }

    [[nodiscard]] bool operator<(Timestamp b) const { return epoch_ < b.epoch_; }
    [[nodiscard]] bool operator>(Timestamp b) const { return epoch_ > b.epoch_; }
    [[nodiscard]] bool operator<=(Timestamp b) const { return epoch_ <= b.epoch_; }
    [[nodiscard]] bool operator>=(Timestamp b) const { return epoch_ >= b.epoch_; }
    [[nodiscard]] bool operator==(Timestamp b) const { return epoch_ == b.epoch_; }
    [[nodiscard]] bool operator!=(Timestamp b) const { return epoch_ != b.epoch_; }

    [[nodiscard]] Duration operator-(Timestamp t) const { return Duration(epoch_ - t.epoch_); }

    [[nodiscard]] Timestamp operator+(Duration d) const { return Timestamp(epoch_ + d.duration_); }

    [[nodiscard]] Timestamp operator-(Duration d) const { return Timestamp(epoch_ - d.duration_); }

    Timestamp& operator+=(Duration d) {
        epoch_ += d.duration_;
        return *this;
    }

    Timestamp& operator-=(Duration d) {
        epoch_ -= d.duration_;
        return *this;
    }

    //! Returns a string to represent the time and date.
    //! The time is printed as UTC time, in "C" locale (English).
    // See https://www.boost.org/doc/libs/1_81_0/doc/html/date_time/date_time_io.html#date_time.format_flags
    // for format defintion.
    [[nodiscard]] std::string asString(const char* format = "%Y.%m.%d - %H:%M:%S.%f") const;

    // convert from string format, e.g. "2002-01-20 23:59:59.000"
    [[nodiscard]] static std::optional<Timestamp> fromString(const std::string& ts);

    // Conversion from std::chrono::time_point is transparent
    using time_point =
        std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>;

    // cppcheck-suppress noExplicitConstructor
    Timestamp(time_point t) : epoch_(t.time_since_epoch().count()) {}
    operator time_point() const { return time_point(std::chrono::microseconds(epoch_)); }

private:
    explicit Timestamp(int64_t t) : epoch_(t) {}

    // Unit: microseconds (1e-6 seconds) elapsed since Jan. 1st 1970, UTC.
    int64_t epoch_;
};

std::ostream& operator<<(std::ostream& o, const Timestamp& t);

#endif
