// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#include <gtest/gtest.h>

#include <chrono>
#include "timestamp.h"

TEST(TimestampTest, ConstructNow) {
    int numSucceded =0;
    int numFailed = 0;

    // Because the OS scheduler could interrupt this test
    // just between the 2 time readings, we run it multiple times
    // and allow for a few failures.
    for (int i = 0; i < 10; ++i) {
        Timestamp timeAtStart(Timestamp::now());
        Timestamp alsoTimeAtStart;

        // We expect a timer accuracy of .1 millisecond.
        double difference = (alsoTimeAtStart - timeAtStart).seconds();
        if (difference >= 0 && difference < 1e-4) {
            ++numSucceded;
        } else {
            ++numFailed;
        }
    }
    // We should succeed at least once...
    EXPECT_GT(numSucceded, 0);
    // ... and succeed more often than failing.
    EXPECT_LT(numFailed, numSucceded);
}

TEST(TimestampTest, CheckSmallestIncrement) {
    Timestamp timeAtStart(Timestamp::now());

    Timestamp aBitLater;
    while (!(timeAtStart < aBitLater)) { aBitLater = Timestamp::now(); }

    // We expect a timer accuracy of .1 millisecond.
    double difference = (aBitLater - timeAtStart).seconds();
    EXPECT_LT(0, difference);
    EXPECT_NEAR(0.0, difference, 1e-3);
}

TEST(TimestampTest, OperatorsWork)
{
    Timestamp a = Timestamp::microSecondsSince1970(12345612);
    Timestamp b = Timestamp::microSecondsSince1970(98877663);
    EXPECT_TRUE(a == a);
    EXPECT_FALSE(a == b);

    EXPECT_TRUE(b != a);
    EXPECT_FALSE(a != a);

    EXPECT_TRUE(a < b);
    EXPECT_FALSE(b < a);
    EXPECT_FALSE(a < a);

    EXPECT_TRUE(a <= b);
    EXPECT_TRUE(a <= a);
    EXPECT_FALSE(b <= a);
}

TEST(TimestampTest, WaitLoop) {
    Timestamp timeAtStart(Timestamp::now());
    Duration targetWaitTime = Duration::milliSeconds(30);
    Timestamp later(Timestamp::now() + targetWaitTime);

    while (Timestamp::now() < later) {}
    Timestamp after(Timestamp::now());

    Duration effectiveWaitTime(after - timeAtStart);

    // In theory, we should have waited for 30 milliseconds.
    // Effective wait time might be larger because we might have been pre-empted.
    EXPECT_GE(effectiveWaitTime, targetWaitTime);
}

TEST(TimestampTest, OneSecondConstructors) {
    // Different ways of measuring a second.
    Duration delta(Duration::milliSeconds(1000));
    Duration delta2(Duration::seconds(1.0));
    Duration delta3(Duration::microSeconds(1000000));

    EXPECT_EQ(1000000, delta.microSeconds());
    EXPECT_EQ(1000000, delta2.microSeconds());
    EXPECT_EQ(1000000, delta3.microSeconds());
}

TEST(TimestampTest, Arithmetic) {
    Timestamp timeAtStart(Timestamp::now());
    Duration delta(Duration::milliSeconds(1000));
    Timestamp later(timeAtStart + delta);
    Timestamp copy(timeAtStart);
    copy += delta;
    EXPECT_FALSE(copy < later);
    EXPECT_FALSE(later < copy);
}

TEST(TimestampTest, Sleep) {
    const Duration wantedWaitTime(Duration::milliSeconds(12));

    Timestamp before(Timestamp::now());
    wantedWaitTime.sleep();
    Timestamp after(Timestamp::now());

    Duration actualWaitTime = after - before;

    // We want to be sure we waited at least the specified time.
    EXPECT_LE(wantedWaitTime.microSeconds(), actualWaitTime.microSeconds());
}

TEST(TimestampTest, DurationStdChronoConv) {
    const std::chrono::microseconds delay(350000);
    const Duration sameDelay(delay);
    EXPECT_EQ(sameDelay.microSeconds(), delay.count());
    EXPECT_EQ(Duration::microSeconds(delay.count()).microSeconds(), delay.count());

    const std::chrono::microseconds asChrono(sameDelay);
    EXPECT_EQ(asChrono.count(), delay.count());
}

template <typename Dur>
void templatedDurationStdChronoConvFromMillisecondsAndSecondsTest(Dur chronoDur) {
    const Duration dur(chronoDur);
    EXPECT_EQ(dur.microSeconds(), std::chrono::microseconds(chronoDur).count());
}

TEST(TimestampTest, DurationStdChronoConvFromMillisecondsAndSeconds) {
    templatedDurationStdChronoConvFromMillisecondsAndSecondsTest(std::chrono::milliseconds(100));
    templatedDurationStdChronoConvFromMillisecondsAndSecondsTest(std::chrono::seconds(2));
}

TEST(TimestampTest, TimestampStdChronoConv) {
    const Timestamp now = Timestamp::now();
    const std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds> asTimePoint(
        now);
    const Timestamp backToTimestamp = asTimePoint;

    EXPECT_EQ(backToTimestamp.microSecondsSince1970(), now.microSecondsSince1970());
}

TEST(TimestampTest, TimestampUnitConversionsPreservePrecision) {
    {
        double epoch = 1500;
        EXPECT_EQ(1.5, Timestamp::microSecondsSince1970(epoch).milliSecondsSince1970()); 
    }
    {
        double epoch = 1500 * 1000;
        EXPECT_EQ(1.5, Timestamp::microSecondsSince1970(epoch).secondsSince1970());
    }
}

TEST(TimestampTest, TimestampConstructorsUsecorrectUnits) {
    {
        double milliseconds = 1;
        EXPECT_EQ(1000, Timestamp::milliSecondsSince1970(milliseconds).microSecondsSince1970()); 
    }
    {
        double seconds = 1;
        EXPECT_EQ(1000 * 1000, Timestamp::secondsSince1970(seconds).microSecondsSince1970());
    }
}

TEST(TimestampTest, asStringReturnsSpecialCaseWithMaxEpoch) {
    EXPECT_EQ(Timestamp::largestPossible().asString(), "[largest possible epoch]");
}

TEST(TimestampTest, asStringReturnsFormattedDateTimeIn2023) {
    EXPECT_EQ(Timestamp::microSecondsSince1970(1681668256123456).asString(), "2023.04.16 - 18:04:16.123456");
}

TEST(TimestampTest, fromStringReturnsNulloptWhenInvalidString) {
    EXPECT_FALSE(Timestamp::fromString("").has_value());
    EXPECT_FALSE(Timestamp::fromString("nothing").has_value());
    EXPECT_FALSE(Timestamp::fromString("2023-05-02 x7:03:02").has_value());
}

TEST(TimestampTest, fromStringParsesAsStringWithMilliseconds) {
    const Timestamp refTs = Timestamp::microSecondsSince1970(1681668256123456);
    const std::string stringWithMilliseconds = refTs.asString("%Y-%m-%d %H:%M:%S.%f");
    
    const std::optional<Timestamp> parsedTs = Timestamp::fromString(stringWithMilliseconds);
    EXPECT_TRUE(parsedTs.has_value());
    EXPECT_EQ(parsedTs->milliSecondsSince1970(), refTs.milliSecondsSince1970());
}

TEST(TimestampTest, fromStringParsesAsStringWithSeconds) {
    const Timestamp refTs = Timestamp::microSecondsSince1970(1681668256123456);
    const std::string stringWithSeconds = refTs.asString("%Y-%m-%d %H:%M:%S");
    
    const std::optional<Timestamp> parsedTs = Timestamp::fromString(stringWithSeconds);
    EXPECT_TRUE(parsedTs.has_value());
    EXPECT_EQ(parsedTs->secondsSince1970(), static_cast<int>(refTs.secondsSince1970()));
}

TEST(TimestampTest, DurationShouldFormatProperly) {
    Duration zero = Duration::milliSeconds(0);
    EXPECT_NE(std::string::npos, zero.format().find("0s"));
    Duration negative = Duration::milliSeconds(-10);
    EXPECT_EQ("-", negative.format().substr(0, 1));

    Duration twoHours = Duration::seconds(2 * 60 * 60);
    EXPECT_EQ("(2h)", twoHours.format());
}
