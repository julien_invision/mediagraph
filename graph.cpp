// Copyright (c) 2012-2013, Aptarism SA.
//
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
// * Neither the name of the University of California, Berkeley nor the
//   names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#include "graph.h"

#include <cassert>
#include <string>
#include <set>

#include "stream.h"
#include "stream_reader.h"

using std::string;

namespace media_graph {
Graph::Graph() : started_(false), stopping_(false) {
    addGetProperty(this, "atLeastOneNodeRunning", &Graph::atLeastOneNodeRunning);
}

bool allPinsAndStreamsNotNullWithUniqueNames(const std::shared_ptr<NodeBase>& node)
{
    // We expect this function to be called whenever a node is added to a graph, so there's
    // no need to check pin/stream names again if the node is already in a graph. 
    if (!node->graphs().empty())
    {
        return true;
    }
    
    // We rely on pins and streams being uniquely identified by their node and
    // name so duplicate stream or pin names are not allowed
    std::set<std::string> uniqueNames;
    for (int i = 0; i < node->numInputPin(); ++i)
    {
        const auto* pin = node->constInputPin(i);
        if (pin == nullptr || !uniqueNames.insert(pin->name()).second)
        {
            return false;
        }
    }
    uniqueNames.clear();

    for (int i = 0; i < node->numOutputStream(); ++i)
    {
        const auto* stream = node->constOutputStream(i);
        if (stream == nullptr || !uniqueNames.insert(stream->streamName()).second)
        {
            return false;
        }
    }
    return true;
}

bool Graph::canAddNodeWithName(const std::string& name) const
{
    // We do not accept nodes with no name or the same name as an existing node.
    return !name.empty() && !lockedGetNodeByName(name);
}

bool Graph::addNode(const std::string& name, const std::shared_ptr<NodeBase>& node) {
    
    if (node == nullptr) { return false; }

    std::lock_guard<std::mutex> lock(mutex_);

    if (!canAddNodeWithName(name)) { return false; }
    
    if (!allPinsAndStreamsNotNullWithUniqueNames(node)) { return false; }
    
    if (!node->setNameAndAttach(name, this)) { return false; }

    nodes_[name] = node;
    return true;
}

bool Graph::addNode(const std::shared_ptr<NodeBase>& node) {
    
    if (node == nullptr) { return false; }
    
    std::lock_guard<std::mutex> lock(mutex_);

    if (!canAddNodeWithName(node->name())) {
        return false;
    }

    if (!allPinsAndStreamsNotNullWithUniqueNames(node)) { return false; }

    if (!node->attach(this)) { return false; }

    nodes_[node->name()] = node;
    return true;
}

void Graph::removeNode(const std::string& name) {
    std::unique_lock<std::mutex> lock(mutex_);

    auto it = nodes_.find(name);
    if (it != nodes_.end()) {
        auto curNode = it->second;
        nodes_.erase(it);

        curNode->disconnectAllPins();
        curNode->disconnectAllStreams();

        // Make sure to unlock before "node" is destroyed.
        lock.unlock();
    }
}

std::shared_ptr<NodeBase> Graph::getNodeByName(const std::string& name) const {
    std::lock_guard<std::mutex> lock(mutex_);
    return lockedGetNodeByName(name);
}

std::shared_ptr<NodeBase> Graph::lockedGetNodeByName(const std::string& name) const {
    auto it = nodes_.find(name);
    if (it != nodes_.end()) { return it->second; }
    return std::shared_ptr<NodeBase>();
}

bool Graph::start() {
    if (isStarted()) { return true; }

    std::lock_guard<std::mutex> lock(mutex_);
    for (const auto& it : nodes_) {
        if (!it.second->start()) {
            // TODO: give a meaningful error.
            lockedStop();  // stop potentially started nodes.
            return false;
        }
    }
    started_ = true;
    return true;
}

bool Graph::isStarted() const {
    return started_;
}

bool Graph::atLeastOneNodeRunning() const {
    for (const auto& it : nodes_) {
        if (it.second->isRunning()) { return true; }
    }
    return false;
}

void Graph::waitUntilStopped() const {
    for (const auto& it : nodes_) { it.second->waitUntilStopped(); }
}

void Graph::stop() {
    // Prevent deadlocks
    if (stopping_) { return; }

    std::lock_guard<std::mutex> lock(mutex_);
    stopping_ = true;
    lockedStop();
    stopping_ = false;
}

void Graph::lockedStop() {
    // First close all streams.
    for (const auto& it : nodes_) { it.second->closeConnectedPins(); }

    // Then, stop all nodes, and maybe wait for them to finish.
    // Since all streams are now closed, stopping nodes won't deadlock,
    // whatever the order of stopping.
    for (const auto& it : nodes_) { it.second->stop(); }
    started_ = false;
}

void Graph::clear() {
    stop();

    while (!nodes_.empty()) { nodes_.begin()->second->detach(this); }
}

bool Graph::connect(NamedStream* stream, NamedPin* pin) {
    if (!stream || !pin) { return false; }
    return pin->connect(stream);
}

bool Graph::connect(const std::shared_ptr<NodeBase>& source, const std::string& streamName,
                    const std::shared_ptr<NodeBase>& dest, const std::string& pinName) {
    if (!source || !dest) { return false; }

    // If we don't have a node with this name and cannot add it or there is a
    // different node with this name in the graph, we cannot properly keep 
    // track of node/graph ownership.
    const auto initialNodeWithSourceName = getNodeByName(source->name());
    if ((!initialNodeWithSourceName && !addNode(source)) || (initialNodeWithSourceName && initialNodeWithSourceName != source))
    {
        return false;
    } 

    // Same as previous check, but also detach source on failure if source was 
    // not originally part of the graph to prevent side effects. 
    const auto initialNodeWithDestName = getNodeByName(dest->name()); 
    if ((!initialNodeWithDestName && !addNode(dest)) || (initialNodeWithDestName && initialNodeWithDestName != dest))
    {
        if (initialNodeWithSourceName != source)
        {
            source->detach(this);
        }
        return false;
    } 

    NamedStream* stream = source->getOutputStreamByName(streamName);
    NamedPin* pin = dest->getInputPinByName(pinName);

    return connect(stream, pin);
}

bool Graph::connect(const std::string& source_name, const std::string& streamName,
                    const std::string& dest_name, const std::string& pinName) {
    return connect(getNodeByName(source_name), streamName, getNodeByName(dest_name), pinName);
}

std::shared_ptr<NodeBase> Graph::node(int num) const {
    auto it = nodes_.begin();
    for (int i = 0; i < num; ++i) {
        if (it == nodes_.end()) { return nullptr; }
        ++it;
    }
    return it->second;
}

}  // namespace media_graph
